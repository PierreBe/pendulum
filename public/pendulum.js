'use strict'
document.addEventListener('DOMContentLoaded', main)
let ctx

function main() {
    document.removeEventListener('DOMContentLoaded', main)
    // fix_dpi()
    ctx = canvas.getContext('2d')
    // document.createElement('canvas').
    // let origin = [[canvas.width / 2], [0]]
    // let weight = [[0], [canvas.height * 3 / 4]]
    // drawLines(origin, add(origin, weight))
    // drawCircle(add(origin, weight), canvas.height * 1 / 5)
    ctx.translate(canvas.width / 2, 0)
    let origin = [[0], [0]]
    let weight = [[0], [canvas.height * 3 / 4]]
    weight = rotate(weight, Math.PI / 4)
    drawLines(origin, weight)
    drawCircle(weight, canvas.height * 1 / 5)
}

function drawLines(...vectors) {
    ctx.moveTo(vectors[0][0][0], vectors[0][1][0])
    for (let i = 1; i < vectors.length; i++) {
        ctx.lineTo(vectors[i][0][0], vectors[i][1][0])
    }
    ctx.stroke()
}

function drawCircle(center, radius) {
    ctx.beginPath()
    ctx.arc(center[0][0], center[1][0], radius, 0, 2 * Math.PI)
    ctx.stroke()
}

// ! defer in html
/* //get the canvas, canvas context, and dpi
let canvas = document.getElementById('canvas'),
    ctx = canvas.getContext('2d'),
    dpi = window.devicePixelRatio;
function fix_dpi() {
//create a style object that returns width and height
  let style = {
    height() {
      return +getComputedStyle(canvas).getPropertyValue('height').slice(0,-2);
    },
    width() {
      return +getComputedStyle(canvas).getPropertyValue('width').slice(0,-2);
    }
  }
//set the correct attributes for a crystal clear image!
  canvas.setAttribute('width', style.width() * dpi);
  canvas.setAttribute('height', style.height() * dpi);
}
function draw() {
  //call the dpi fix every time 
  //canvas is redrawn
  fix_dpi();
  
  //draw stuff!
  ctx.strokeRect(30,30,100, 100);
  ctx.font = "30px Arial";
  ctx.fillText("Demo!", 35, 85);
  requestAnimationFrame(draw);
}
requestAnimationFrame(draw); */

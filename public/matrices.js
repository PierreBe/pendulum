'use strict'
function add(mat1, mat2) {
    let out = []
    for (let i = 0; i < mat1.length; i++) {
        out[i] = []
        for (let j = 0; j < mat1[0].length; j++) {
            out[i][j] = mat1[i][j] + mat2[i][j]
        }
    }
    return out
}

function dot(mat1, mat2) {
    let out = []
    for (let i = 0; i < mat1.length; i++) {
        out[i] = []
        for (let j = 0; j < mat1[0].length; j++) {
            out[i][j] = 0
            for (let k = 0; k < mat1[0].length; k++) {
                out[i][j] += mat1[i][k] * mat2[k][j]
            }
        }
    }
    return out
}

function rotate(vector, angle) {
    let r = [
        [Math.cos(angle), -Math.sin(angle)],
        [Math.sin(angle), Math.cos(angle)]
    ]
    return dot(r, vector)
}
